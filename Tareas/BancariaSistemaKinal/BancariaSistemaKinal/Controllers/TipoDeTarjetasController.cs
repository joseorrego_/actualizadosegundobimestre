﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BancariaSistemaKinal.Models;

namespace BancariaSistemaKinal.Controllers
{
    public class TipoDeTarjetasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [Authorize(Roles = "Admin, User")]
        // GET: /TipoDeTarjetas/
        public ActionResult Index()
        {
            return View(db.TipoTarjetas.ToList());
        }

        // GET: /TipoDeTarjetas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoTarjeta tipotarjeta = db.TipoTarjetas.Find(id);
            if (tipotarjeta == null)
            {
                return HttpNotFound();
            }
            return View(tipotarjeta);
        }

        [Authorize(Roles = "Admin")]
        // GET: /TipoDeTarjetas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /TipoDeTarjetas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,TipoDeTarjeta")] TipoTarjeta tipotarjeta)
        {
            if (ModelState.IsValid)
            {
                db.TipoTarjetas.Add(tipotarjeta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipotarjeta);
        }

        [Authorize (Roles="Admin")]
        // GET: /TipoDeTarjetas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoTarjeta tipotarjeta = db.TipoTarjetas.Find(id);
            if (tipotarjeta == null)
            {
                return HttpNotFound();
            }
            return View(tipotarjeta);
        }

        // POST: /TipoDeTarjetas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,TipoDeTarjeta")] TipoTarjeta tipotarjeta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipotarjeta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipotarjeta);
        }

        [Authorize(Roles = "Admin")]
        // GET: /TipoDeTarjetas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoTarjeta tipotarjeta = db.TipoTarjetas.Find(id);
            if (tipotarjeta == null)
            {
                return HttpNotFound();
            }
            return View(tipotarjeta);
        }

        // POST: /TipoDeTarjetas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoTarjeta tipotarjeta = db.TipoTarjetas.Find(id);
            db.TipoTarjetas.Remove(tipotarjeta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
