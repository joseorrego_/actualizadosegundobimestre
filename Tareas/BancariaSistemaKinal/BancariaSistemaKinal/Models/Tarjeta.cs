﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BancariaSistemaKinal.Models
{
    public class Tarjeta
    {
        public int ID { get; set; }
        [Display (Name="Numero Tarjeta")]
        public int NumeroTarjeta { get; set;}
        [Display (Name="PIN")]
        public int Pin { get; set; }
        [Display (Name="Fecha de creacion")]
        public string Creacion { get; set; }
        [Display (Name="Fecha de validez")]
        public string Validez { get; set; }
        [Display (Name="Monto con el cual comienza")]
        public int Monto { get; set; }
        [Display (Name="Estado de la tarjeta")]
        public string Estado { get; set; }

        public int TipoTarjetaID { get; set; }
        public TipoTarjeta TipoTarjeta { get; set; }
    }
}