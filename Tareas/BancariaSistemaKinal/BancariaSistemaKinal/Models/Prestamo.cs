﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BancariaSistemaKinal.Models
{
    public class Prestamo
    {
        public int Id { get; set; }
        [Display (Name="Meses del prestamo")]
        public string Meses { get; set; }
        [Display(Name="Monto del prestamo")]
        public string Monto { get; set; }
        [Display (Name="Plazo del prestamo")]
        public string Plazo { get; set; }
        [Display (Name="Dinero solicitado")]
        public string DineroAPedir { get; set; }
        [Display (Name="Fecha que lo solicita")]
        public string FechaQueLoSolicita { get; set; }
    }
}