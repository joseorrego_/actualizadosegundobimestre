﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BancariaSistemaKinal.Models
{
    public class TipoCuenta
    {
        public int ID { get; set; }
        [Display (Name="Tipo de Cuenta")]
        public string Tipo { get; set; }

    }
}