﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BancariaSistemaKinal.Models
{
    public class Transaccion
    {
        public int ID { get; set; }
        [Display (Name="Fecha de la transaccion")]
        public string Fecha { get; set; }
        [Display (Name="Saldo Inicial")]
        public int SaldoInicial { get; set; }
        [Display (Name="Saldo Retirado")]
        public int SaldoRetirado { get; set; }

    }
}