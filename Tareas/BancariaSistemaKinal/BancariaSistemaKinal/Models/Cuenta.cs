﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BancariaSistemaKinal.Models
{
    public class Cuenta
    {
        [Key]
        public int ID { get; set;}
        [Display (Name="Cuenta")]
        public string Nombre { get; set; }
        [Display (Name="Estado de la cuenta")]
        public string Estado { get; set; }
        [Display (Name="Saldo Inicial")]
        public int SaldoInicial { get; set; }
        [Display (Name="Numero de la Cuenta")]
        public int Numerodelacuenta { get; set; }

        public int ClienteID { get; set; }
        public Cliente Cliente { get; set; }

        public int TipoCuentaID { get; set;}
        public TipoCuenta TipoCuenta { get; set; }

    }
}